const http = require('http');
const fs = require('fs')
const server = http.createServer((request,response)=>{
  const {url,method,headers} = request;
  if(url === '/'&&method==='GET'){
    fs.readFile('01/index.html',(err,data)=>{
      if(err)console.log(err)
      response.stateCode = 200;
      response.setHeader = {
        'Content-Type':'text/html'
      }
      response.end(data)
    })
  }else if(url === '/user' && method==='GET'){
    // response.stateCode = 200;
    // response.setHeader = {
    //   'Content-Type':'application/json'
    // }
    response.writeHead(200,{
      'Content-Type':'application/json'
    })
    response.end(JSON.stringify({
      name:'123'
    }))
  }else if(method==='GET' && headers.accept.indexOf('image/*') >=0){
    console.log(url)
    fs.createReadStream('./'+url).pipe(response);
  }
})
server.listen(3001)
