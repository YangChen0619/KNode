// const {promisify} = require('./promisify.js')
const {promisify} = require('util')
const download = promisify(require('download-git-repo'));
const ora = require('ora');

module.exports.clone = async function (repo,dest) {
  const process = ora(`${repo}正在下载中`)
  process.start();
  try {
    await download(repo,dest)
  }catch (e) {
    process.fail();
  }
  process.succeed();

}
