module.exports.promisify = function (fn) {
  return function (...args) {
    return new Promise((resolve,reject)=>{
      args.push(function(err,...args){
        if(err){
          reject(err)
        }else{
          resolve(...args)
        }
      })
      fn.apply(null,args)
    })
  }
}
