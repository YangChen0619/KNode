const Koa = require('koa')
const fs = require('fs')
const app = new Koa();
app.use(async (ctx,next) => {
    const start = new Date().getTime();
    console.log(`开始1`)
    await next()
    console.log(`结束1`)
});
app.use(async (ctx,next) => {
    console.log(`开始2`)
    ctx.body = {
      name:'123'
    }
    await next()
    console.log(`结束2`)
});
app.use(async (ctx,next) => {
    console.log(`开始3`)
    await next()
    console.log(`结束3`)
});

app.listen(3000);
