// const add = (x,y)=>x+y;
// const square = z => z*z;
// const square2 = z => z*z;
// // const compose = (fn1,fn2) => (...args) => fn2(fn1(...args));
// const compose = (...[first,...other]) =>(...args) =>{
//   let ret = first(...args)
//   other.forEach(fn => {
//     ret = fn(ret);
//   })
//   return ret
// }
// console.log(compose(add,square,square2)(1,2))

const compose = function (middlewares) {
  return function () {
    return dispath(0);
    function dispath(i) {
      let fn = middlewares[i]
      if(!fn){
        return Promise.resolve()
      }
      return Promise.resolve(fn(function next() {
        return dispath(i+1)
      }))
    }
  }

}
const fn1 = async function (next) {
  console.log('1')
  await next()
  console.log('1')
}
const fn2 = async function (next) {
  console.log('2')
  await next()
  console.log('2')
}
const fn3 = async function (next) {
  console.log('3')
  await next()
  console.log('3')
}

const middlewares = [fn1,fn2,fn3]
const finalFn = compose(middlewares)
finalFn()
