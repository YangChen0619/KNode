const http = require('http');
const context = require('./context.js')
const request = require('./request.js')
const response = require('./response.js')
module.exports = class KKB{
  constructor(){
    this.middlewares = [];
  }
  listen (...args) {
    const server = http.createServer(async (req,res)=>{
      const ctx = this.createContext(req,res)
      const fn = this.compose(this.middlewares);
      await fn(ctx)
      res.end(ctx.body)
    })
    server.listen(...args)
  }
  use (middleware){
    this.middlewares.push(middleware)
  }
  createContext (req,res){
    const ctx = Object.create(context);
    ctx.request = Object.create(request);
    ctx.response = Object.create(response);

    ctx.req = ctx.request.req = req
    ctx.res = ctx.request.res = res
    return ctx
  }
  compose (middlewares) {
    return function (ctx) {
      return dispath(0);
      function dispath(i) {
        let fn = middlewares[i]
        if(!fn){
          return Promise.resolve()
        }
        return Promise.resolve(fn(ctx,function next() {
          return dispath(i+1)
        }))
      }
    }

  }
}
