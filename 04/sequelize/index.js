( async()=>{
  const Sequelize = require('sequelize');
  const sequelize = new Sequelize('selecttest','root','yangchen',{
    host:'localhost',
    dialect:'mysql'
  })
  const Fruit = sequelize.define('Fruit',{
    id:{
      type:Sequelize.DataTypes.UUID,
      defaultValue: Sequelize.DataTypes.UUIDV1,
      primaryKey:true
    },
    name:{type:Sequelize.STRING(20), allowNull:false},
    price:{type:Sequelize.FLOAT,allowNull:false},
    stock:{type:Sequelize.INTEGER,defaultValue:0}
  },{
    timestamps:false,
    tableName:'tb_fruit'
  })
  let ret = await Fruit.sync({
    force:false
  });
  console.log('连接成功',ret)
  ret = await Fruit.create({
    name:'香蕉',
    price:3.5
  })
  console.log('插入成功',ret)
  ret = await Fruit.update({price:4},{
    where:{
      name:'香蕉'
    }
  })
  console.log('更新成功',ret)
  const Op = Sequelize.Op;
  ret = await Fruit.findAll({
    where:{
      price:{
        [Op.lt]:5,
        [Op.gt]:3.5
      }
    }
  })
  console.log('查询成功',JSON.stringify(ret,'','\t'))
})()
